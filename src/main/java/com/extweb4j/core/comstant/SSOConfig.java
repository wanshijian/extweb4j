
package com.extweb4j.core.comstant;

import java.nio.charset.Charset;


/**
 * <p>
 * SSO 配置文件解析
 * 
 * @author hubin
 * @Date 2015-12-05
 */
public class SSOConfig {
	private static final String SSO_ENCODING = "UTF-8";
	
	/**
	 * Charset 类型编码格式
	 */
	public static final Charset CHARSET_ENCODING = Charset.forName(getSSOEncoding());
	
	public static final String CUT_SYMBOL = "#";
	
	public  static String getSSOEncoding(){
		return SSO_ENCODING;
	}
}
