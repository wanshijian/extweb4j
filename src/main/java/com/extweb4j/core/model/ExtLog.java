package com.extweb4j.core.model;

import java.util.List;

/**
 * 日志
 * @author Administrator
 *
 */
public class ExtLog extends CoreModel<ExtLog>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static ExtLog dao = new ExtLog();
	

	/**
	 * 分组统计查询日志
	 * @param limit
	 * @return
	 */
	
	public List<ExtLog> findExtLogDataBy(int limit){
		
		StringBuffer sqlBuffer = new StringBuffer("SELECT cdate,COUNT(cdate) ct");
		sqlBuffer.append(" FROM (SELECT  SUBSTRING(e.`create_time`,1,10) cdate FROM `ext_log` e) t");
		sqlBuffer.append("	GROUP BY cdate");
		sqlBuffer.append("	ORDER BY cdate ASC");
		sqlBuffer.append("	LIMIT ?");
		
		return ExtLog.dao.find(sqlBuffer.toString(),limit);
	}

}
