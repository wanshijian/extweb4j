package com.extweb4j.core.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
/**
 * 菜单
 * @author Administrator
 *
 */
public class ExtMenu extends CoreModel<ExtMenu>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static ExtMenu dao = new ExtMenu();
	
	/**
	 * 根据父菜单ID获取子菜单
	 * @param pid
	 * @return
	 */
	public  List<ExtMenu> getMenuTreeByPid(String pid) {
		// TODO Auto-generated method stub
		
		StringBuffer sqlBuffer = new StringBuffer("SELECT ext.*,ext2.text ptext FROM ext_menu ext");
		sqlBuffer.append(" LEFT JOIN ext_menu ext2 ON ext.pid = ext2.id");
		sqlBuffer.append("  WHERE ext.pid = ? ORDER BY  ext.sort ASC");
		return ExtMenu.dao.find(sqlBuffer.toString(), pid);
	}
	/**
	 * 根据父菜单ID获取子菜单
	 * @param pid
	 * @return
	 */
	public  List<ExtMenu> getMenuTreeByPid(String pid,int maxDeep) {
		// TODO Auto-generated method stub
		String sql = "SELECT ext.* FROM ext_menu ext WHERE ext.pid = ? AND ext.deep <= ? ORDER BY  ext.sort ASC ";
		
		return ExtMenu.dao.find(sql, pid,maxDeep);
	}
	/**
	 * 根据菜单的viewType查询此菜单是否存在
	 * @param viewType
	 * @return
	 */
	public  boolean hasViewType(String viewType) {
		// TODO Auto-generated method stub
		return ExtMenu.dao.findFirst("SELECT ext.* FROM ext_menu ext WHERE ext.view_type = ?",viewType) != null;
	}
	/**
	 * 关键词查询
	 * @param q
	 * @return
	 */
	public List<ExtMenu> getMenuTreeByKeyword(String q) {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer("SELECT t.id, t.text,t.deep  FROM ext_menu t ");
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(q)){
			sb.append(" WHERE  t.text LIKE  CONCAT('%',?,'%')");
			params.add(q);
		}
		sb.append(" ORDER BY sort ASC");
		return  ExtMenu.dao.find(sb.toString(),params.toArray());
	}
	/**
	 * 权限查询
	 * @param pid
	 * @param userId
	 * @return
	 */
	public  List<ExtMenu> getAuthMenu(String pid,String roleId) {
		// TODO Auto-generated method stub
		String sql1 = " (SELECT  * FROM ext_menu m WHERE  m.pid = ? ) t1";
		String sql2 = " (SELECT rm.menu_id FROM ext_role_menu rm WHERE rm.role_id=? ) t2";
		StringBuffer sqlBuffer = new StringBuffer("SELECT * FROM ").append(sql1).append(" LEFT JOIN ").append(sql2).append(" ON t1.id = t2.menu_id").append(" ORDER BY t1.sort ASC");
		List<ExtMenu>  list = ExtMenu.dao.find(sqlBuffer.toString(), pid,roleId);
		for(ExtMenu m: list){
			if(StringUtils.isNotBlank(m.getStr("menu_id"))){
				m.put("checked", true);
			}else{
				m.put("checked", false);
			}
		}
		return list;
	}
	/**
	 * 验证是否有子节点
	 * @param id
	 * @return
	 */
	public boolean hasChindNode(String id) {
		// TODO Auto-generated method stub
		List<ExtMenu> list = ExtMenu.dao.find("SELECT ext.id FROM ext_menu ext where ext.pid = ? ",id) ;
		return list!=null && list.size()>0;
	}
	
	public List<ExtMenu> findByPidAndIds(String pid, Object[] ids) {
		// TODO Auto-generated method stub
		
		StringBuffer sb = new StringBuffer();
		
		for(int i=0;i<ids.length;i++){
			sb.append("?").append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		
		String sql = "SELECT * FROM ext_menu m WHERE m.pid  = '"+pid+"'  AND m.id IN ( "+sb.toString()+" )"
				+ "AND m.status = 1 ORDER BY m.sort ASC";
		
		return ExtMenu.dao.find(sql,ids);
	}
	public List<ExtMenu> findActionBy(String uid) {
		// TODO Auto-generated method stub
		
		StringBuffer sb = new StringBuffer("SELECT m.action  FROM ext_user_role ur ");
		sb.append(" LEFT JOIN ext_role_menu rm ON ur.role_id = rm.role_id");
		sb.append(" LEFT JOIN ext_menu m ON m.id = rm.menu_id");
		sb.append(" WHERE m.action IS NOT NULL ");
		sb.append(" AND ur.user_id = ?");
		return ExtMenu.dao.find(sb.toString(), uid);
	}
	
}
