package com.extweb4j.web.plugin;

import com.extweb4j.core.model.ExtDept;
import com.extweb4j.core.model.ExtIcon;
import com.extweb4j.core.model.ExtLog;
import com.extweb4j.core.model.ExtMenu;
import com.extweb4j.core.model.ExtMsg;
import com.extweb4j.core.model.ExtRole;
import com.extweb4j.core.model.ExtRoleMenu;
import com.extweb4j.core.model.ExtUser;
import com.extweb4j.core.model.ExtUserRole;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.IDataSourceProvider;

public class ExtActiveRecordPlugin extends ActiveRecordPlugin{
	
	public ExtActiveRecordPlugin(IDataSourceProvider dataSource) {
		super(dataSource);
		//this.setTransactionLevel(2);
		this.initMapping();
	}
	
	public void initMapping(){
		
		this.setShowSql(PropKit.getBoolean("devMode",false));
		
		this.addMapping("ext_menu", "id",ExtMenu.class);
		this.addMapping("ext_user", "id",ExtUser.class);
		this.addMapping("ext_role", "id",ExtRole.class);
		this.addMapping("ext_user_role", "id",ExtUserRole.class);
		this.addMapping("ext_role_menu", "id",ExtRoleMenu.class);
		this.addMapping("ext_log","id",ExtLog.class);
		this.addMapping("ext_dept","id",ExtDept.class);
		this.addMapping("ext_icon","id",ExtIcon.class);
		this.addMapping("ext_msg","id",ExtMsg.class);
		
	}
}
