package com.extweb4j.test;

import com.extweb4j.core.kit.Pinyin4jKit;


/**
 * 汉字转拼音，能处理多音字
 * 
 * @author feng bingbing
 * 
 */

public class Test {
	public static void main(String[] args) {
		String str = "长沙市长";  
	    
	    String pinyin = Pinyin4jKit.converterToSpell(str);  
	    System.out.println(str+" pin yin ："+pinyin);  
	      
	    pinyin = Pinyin4jKit.converterToFirstSpell(str);  
	    System.out.println(str+" short pin yin ："+pinyin);  
	}
}
